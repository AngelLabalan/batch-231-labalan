console.log("hello world");

//Global Objects
//Arrays

// let students = [
// 	"Tony",
// 	"Peter",
// 	"Wanda",
// 	"Vision",
// 	"Loki"
// ];

// console.log(students);


// // PUSH method  >> add at the end

// students.push("Thor");
// console.log(students);


// // UNSHIFT  >> add at first

// students.unshift("Steve");
// console.log(students);


// // POP method  >> remove at the end

// students.pop();
// console.log(students);


// // SHIFT  >> remove at first

// students.shift();
// console.log(students);



// Difference between splice() and slice() ?

	// .splice() - removes and adds items from a starting index (Mutator Methods)

	// .slice() - copies a portion from starting index and returns new array (non-mutator >> accessor)



// Another Array Methods

	//Iterator methods - loops over the items of an array

//forEach() - loops over in an array and repeats a user-defined function

//map() - loops over in an array and repeats a user-defined function and returns a new array

//every() - loops and checks if all items satisfy a given condition, returns a boolean value.





let arrNum = [15, 20, 25, 30, 11];

//check if every item in arrNum is divisible by 5

let allDivisible;


//FOREACH

arrNum.forEach(num => {
	if(num % 5 === 0){
		console.log(`${num} is divisible by 5`)
	} else {
		allDivisible = false;
	}

	//however, can forEach() return data that will tell us if all numbers/items in our arrNum is divisible by 5?
})

console.log(allDivisible);

arrNum.pop();
arrNum.push(35);
console.log(arrNum);



//EVERY - check all elements based on the existing condition

let divisibleBy5 = arrNum.every(num => {
	console.log(num);
	return num % 5 === 0;
})
console.log(divisibleBy5);
//result: true




// MATH
//MATHEMATICAL CONSTANTS
// 8 predefined properties which can be called via the syntax Math.properties (case sensitive)

console.log(Math);
console.log(Math.E); //Euler's number
console.log(Math.PI); //pi
console.log(Math.SQRT2); //square root of 2
console.log(Math.SQRT1_2); // //square root of 1/2
console.log(Math.LN2); // natural logarithm of 2
console.log(Math.LN10); // natural logarithm of 10
console.log(Math.LOG2E); // base 2 log of E
console.log(Math.LOG10E); // base 10 log of E




// methods for rounding a number to an integer

console.log(Math.round(Math.PI)); // rounds to a nearest integer (no decimal places)
//result: 3

console.log(Math.ceil(Math.PI)); // rounds UP to nearest integer
//result: 4


console.log(Math.floor(Math.PI)); // rounds DOWN to nearest integer
//result: 3


console.log(Math.trunc(Math.PI)); // returns only the integer part (ES6 update)
//result : 3



//methods for returning the square root, min & max of a number

// sqrt >> gets the square root
console.log(Math.sqrt(3.14)); 
//result: 1.77


// min >> gets the minimum or lowest value
console.log(Math.min(-4, -3, -2, -1, 0, 1, 2, 3, 4));
//result: -4


// max >> gets the maximum or greatest value
console.log(Math.max(-4, -3, -2, -1, 0, 1, 2, 3, 4));
//result: 4



//Primitive data types (no functions or methods) = not an object / no prototype result in console

// Ex: string, number, boolean, undefined, null




//methods for strings >> .length, .toUpperCase()
//JS wraps primitives in an object temporarily to perform the operation being called
//Those that are NOT objects can stille behave LIKE objects





// ========================================

/* FUNCTION CODING ACTIVITY */

let students = ["John","Joe","Jane","Jessie"];


//1.

let addToEnd = (array,name) => {
	if(typeof name !== 'string'){
		console.log(`error - can only add strings to an array`);
	} else {
		array.push(name);
		return array
	}
}

//2.

let addToStart = (array,name) => {
	if(typeof name !== 'string'){
		console.log(`error - can only add strings to an array`);
	} else {
		array.unshift(name);
		return array
	}
}

//3.

let elementChecker = (array,name) => {

	if(array.length !== 0){
		let stringy = array.some(student => {
			return array.includes(name);
		});
		console.log(stringy)
	} else {
		return console.log(`error - passed in array is empty`)
	}

}

//4. 
let checkAllStringsEnding = (array, name) => {
	if(array.length === 0){
	return console.log(`error - array must NOT be empty`);
	} else if (typeof array !== 'string' ){
	return console.log(`error - all array elements must be strings`);	
	} else if ( typeof(name) !== 'string'){
	return	console.log(`error - 2nd argument must be of data type string`)
	} else if(name.length > 1){
	return	console.log(`error - 2nd argument must be a single character`)
	} else {
		return true
	};
}



//5.

let stringLengthSorter = (array) => {

	let stringCheck = array.every(string => {
		return typeof string === 'string'
	})
	if(stringCheck){
		return array.sort()
	} else{
		return console.log(`error - all array elements must be strings`);
	}

}




















